/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.model;

/**
 *
 */
public class Unidad {
    private double cantidad=0d;
    private double cantidadConvertida=0d;
    private String unidades="";
    private String unidadesConversion="";


    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad, String unidades ) {
        this.cantidad = cantidad;
        this.unidades=unidades;
    }
    
    public void convertirCantidad(){//REcibe laa cantidad a convertir y hacia que unidad de conversion
        switch (unidades) {
            case "Metro":
                switch (unidadesConversion) {
                    case "Kilometro":
                        cantidadConvertida=cantidad/1000;
                    break;
                    case "Centimetro":  
                        cantidadConvertida=cantidad*100;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*1000;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*1000000;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1000000000;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.000621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*1.09361d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*3.28084d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*39.3701d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;
                    } //Finde switch de las unidades a convertir
                break;
            case "Kilometro":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*1000d;
                    break;
                    case "Centimetro":  
                        cantidadConvertida=cantidad*100000d;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*1000000d;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*1000000000d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1000000000000d;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*1093.61d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*3280.84d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*39370.1d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;
                    } //Finde switch de las unidades a convertir
                break;
            case "Centimetro": 
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.01;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.100000d;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*10;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*10000d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*10000000d;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.00000621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*0.0109361d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*.0328084d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*.393701d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.00000539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;
                    } //Finde switch de las unidades a convertir
                
                break;
            case "Milimetro":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.001;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.000001d;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*0.1;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*1000d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1000000d;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.000000621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*0.00109361d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*.00328084d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*.0393701d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000000539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;
                    } //Finde switch de las unidades a convertir
                break;
            case "Micrometro":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.000001;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.000000001d;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*0.0001;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*.001d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1000d;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.000000000621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*0.00000109361d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*.00000328084d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*.0000393701d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000000000539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;
                    } //Finde switch de las unidades a convertir
                break;
            case "Nanometro":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.000000001;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.000000000001d;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*0.0000001;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*.000001d;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*0.001d;
                        break;
                    case "Milla":
                        cantidadConvertida=cantidad*0.000000000000621371d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*0.00000000109361d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*.00000000328084d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*.0000000393701d;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000000000000539957d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
            case "Milla":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*1609.34;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*1.60934d;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*160934;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*1609000d;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*1609000000d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1609000000000d;
                        break;
                    case "Yarda": 
                        cantidadConvertida=cantidad*1760;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*5280;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*63360;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.868976d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
            case "Yarda": 
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.9144;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.0009144;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*91.44;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*914.4;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*914400d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*914400000d;
                        break;
                    case "Milla": 
                        cantidadConvertida=cantidad*0.000568182d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*3.00000096;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*36.00001152;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.0004937366590756d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
            case "Pie":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.9144;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.0003048;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*30.48;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*304.8;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*304800d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*304800000d;
                        break;
                    case "Milla": 
                        cantidadConvertida=cantidad*0.000189394d;
                        break;
                    case "Yarda":
                        cantidadConvertida=cantidad*0.333333d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*12;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000164579d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
            case "Pulgada":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*0.0254;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*0.0000254;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*2.54;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*25.4;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*25400d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*25400000d;
                        break;
                    case "Milla": 
                        cantidadConvertida=cantidad*0.000015783d;
                        break;
                    case "Yarda":
                        cantidadConvertida=cantidad*0.0277778d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*0.0833333;
                        break;
                    case "Milla Nautica":
                        cantidadConvertida=cantidad*0.000013715d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
            case "Milla Nautica":
                switch (unidadesConversion) {
                    case "Metro":
                        cantidadConvertida=cantidad*1852;
                    break;
                    case "Kilometro":  
                        cantidadConvertida=cantidad*1.852;
                        break;
                    case "Centimetro":
                        cantidadConvertida=cantidad*185200;
                        break;
                    case "Milimetro":
                        cantidadConvertida=cantidad*1852000d;
                        break;
                    case "Micrometro":
                        cantidadConvertida=cantidad*1852000000d;
                        break;
                    case "Nanometro":
                        cantidadConvertida=cantidad*1852000000000d;
                        break;
                    case "Milla": 
                        cantidadConvertida=cantidad*1.15078d;
                        break;
                    case "Yarda":
                        cantidadConvertida=cantidad*2025.37d;
                        break;
                    case "Pie":
                        cantidadConvertida=cantidad*6076.12d;
                        break;
                    case "Pulgada":
                        cantidadConvertida=cantidad*72913.4d;
                        break;
                    default:
                        cantidadConvertida=cantidad;
                        break;

                    } //Finde switch de las unidades a convertir
                break;
          
        }
    }

    public double getCantidadConvertida() {
        return cantidadConvertida;
    }

    public String getUnidades() {
        return unidades;
    }


    public String getUnidadesConversion() {
        return unidadesConversion;
    }

    public void setUnidadesConversion(String unidadesConversion) {
        this.unidadesConversion = unidadesConversion;
    }
    
    
}
