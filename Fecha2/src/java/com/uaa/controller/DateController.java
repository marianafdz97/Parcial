/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller;

import com.uaa.model.Fecha;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author CarlosC
 */
@WebServlet(name = "DateController", urlPatterns = {"/user"})
public class DateController extends HttpServlet {
    long dia=diasEntre("09-10-2017");
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static long diasEntre(String fechaUsuario){
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy");
        Date expDate=null;
        long diff=0;
        long noOfDays=0;
       
        try {
            expDate=formatter.parse(fechaUsuario);
            Date createdDate= new Date();
            diff=createdDate.getTime()-expDate.getTime();
            noOfDays=TimeUnit.DAYS.convert(diff,TimeUnit.MILLISECONDS);
            long a=TimeUnit.DAYS.toDays(noOfDays);
            System.out.println(a);
            System.out.println(noOfDays);
            
        } catch (ParseException ex) {
            Logger.getLogger(DateController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return noOfDays;
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            RequestDispatcher rd = request.getRequestDispatcher("view/user/fecha_info.jsp");
            long dia=diasEntre(request.getParameter("date"));
            Fecha fecha= new Fecha();
            fecha.setDiasTranscurridos((int)dia);
            request.setAttribute("user", fecha);
            rd.forward(request, response);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
