/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.model;

/**
 *
 * 
 */

public class Fecha {
    private String FechaUsuario;
    private long diasTranscurridos;

    public long getDiasTranscurridos() {
        return diasTranscurridos;
    }

    public void setDiasTranscurridos(long diasTranscurridos) {
        this.diasTranscurridos = diasTranscurridos;
    }

    public String getFechaUsuario() {
        return FechaUsuario;
    }

    public void setFechaUsuario(String FechaUsuario) {
        this.FechaUsuario = FechaUsuario;
    }
    
    
}
